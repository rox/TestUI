package ui;

import com.jfoenix.controls.*;
import com.jfoenix.validation.RequiredFieldValidator;
import com.jfoenix.validation.base.ValidatorBase;
import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ldh.common.testui.util.DialogUtil;
import ldh.common.testui.util.JFoenixUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ldh on 2019/12/12.
 */
public class DialogTest extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        StackPane root = new StackPane();

        HBox hBox = new HBox();
        JFXButton jfxButton = new JFXButton("open");
        hBox.getChildren().add(jfxButton);
        jfxButton.setOnAction(e->{
            System.out.println("size:00" + root.getChildren().size());
            List<ValidatorBase> validatorBases = new ArrayList();
            JFXDialog dialog = new JFXDialog();
            dialog.setOverlayClose(false);
            JFXDialogLayout layout = new JFXDialogLayout();
//            layout.setPrefSize(500, 350);

            StackPane stackPane = new StackPane();
            GridPane grid = new GridPane();
            grid.setGridLinesVisible(true);
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(20, 10, 10, 10));

            JFXTextField keyTextField = new JFXTextField();
//        keyTextField.setPromptText("Key");
            RequiredFieldValidator keyValidator = JFoenixUtil.createValidatorBase(new RequiredFieldValidator(), keyTextField);
            keyTextField.getValidators().add(keyValidator);
            validatorBases.add(keyValidator);

            JFXTextArea valueTextArea = new JFXTextArea();
//        valueTextArea.setPromptText("Value");
            RequiredFieldValidator valueValidator = JFoenixUtil.createValidatorBase(new RequiredFieldValidator(), valueTextArea);
            validatorBases.add(valueValidator);

            Label keyLabel = new Label("Key:");
            grid.add(keyLabel, 0, 0);
            grid.add(keyTextField, 1, 0);

            Label valueLabel = new Label("Value:");
            grid.add(valueLabel, 0, 1);
            grid.add(valueTextArea, 1, 1);

            ColumnConstraints columnConstraints1 = new ColumnConstraints(100);
            columnConstraints1.setHalignment(HPos.RIGHT);
            ColumnConstraints columnConstraints2 = new ColumnConstraints(500);
            columnConstraints2.setHgrow(Priority.ALWAYS);
            grid.getColumnConstraints().addAll(columnConstraints1, columnConstraints2);

            JFXButton closeButton = new JFXButton("close");
            closeButton.getStyleClass().add("dialog-accept");
            closeButton.setOnAction(event -> dialog.close());

            stackPane.getChildren().add(grid);

            HBox hBox1 = new HBox();
//            hBox1.getChildren().add(new Label("ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss"));
            hBox1.getChildren().add(grid);
            hBox1.setStyle("-fx-background-color: #b9def0");
            HBox.setHgrow(grid, Priority.ALWAYS);
            JFXButton saveButton = new JFXButton("Add");
            saveButton.getStyleClass().add("dialog-accept");


            layout.getActions().addAll(saveButton, closeButton);
            layout.setBody(hBox1);
            layout.setHeading(new Label("添加键值"));

            dialog.setContent(layout);
            dialog.show(root);
        });

        JFXButton button2 = new JFXButton("open2");
        hBox.getChildren().add(button2);
        button2.setOnAction(e->{
            JFXAlert alert = new JFXAlert((Stage) button2.getScene().getWindow());
            alert.initModality(Modality.APPLICATION_MODAL);
            alert.setOverlayClose(false);
            JFXDialogLayout layout = new JFXDialogLayout();
            layout.setHeading(new Label("Modal Dialog using JFXAlert"));
            layout.setBody(new Label("Lorem ipsum dolor sit amet, consectetur adipiscing elit,"
                    + " sed do eiusmod tempor incididunt ut labore et dolore magna"
                    + " aliqua. Utenim ad minim veniam, quis nostrud exercitation"
                    + " ullamco laboris nisi ut aliquip ex ea commodo consequat."));

//            layout.setBody(new Label("demo!!!!!!!!"));
            JFXButton closeButton = new JFXButton("ACCEPT");
            closeButton.getStyleClass().add("dialog-accept");
            closeButton.setOnAction(event -> alert.hideWithAnimation());
            layout.setActions(closeButton);
            alert.setContent(layout);
            alert.show();
        });

        JFXButton jfxButton3 = new JFXButton("open3");
        hBox.getChildren().add(jfxButton3);
        jfxButton3.setOnAction(e->{
            JFXDialog dialog = new JFXDialog();
//            dialog.setOverlayClose(false);
            dialog.setContent(new Label("Content33"));
            dialog.setOnDialogClosed(e1->{
                System.out.println("size:" + root.getChildren().size());
                for(Node node : root.getChildren()) {
                    if (node instanceof HBox) {
                        System.out.println("size cc:" + ((HBox) node).getChildren().size());
                    } else if (node instanceof JFXDialog) {
                        System.out.println("size cc:" +  ((JFXDialog) node).getContent().getClass());
                    } else {
                        System.out.println("node:" + node.getClass());
                    }
                }
            });
            dialog.show(root);
        });

        initDialog4(hBox, root);

        root.getChildren().add(hBox);
        JFXDecorator jfxDecorator = new JFXDecorator(primaryStage, root);
        Scene scene = new Scene(jfxDecorator, 1200, 700);
        scene.setFill(null);
//        scene.getStylesheets().add("/css/bbb.css");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void initDialog4(HBox hBox, StackPane root) {
        JFXButton jfxButton = new JFXButton("open4");
        hBox.getChildren().add(jfxButton);
        jfxButton.setOnAction(e->{
            JFXAlert<Void> jfxAlert = DialogUtil.createModelDialog(new StackPane(new Label("demo")), "demo", (jfxAlert1, jfxDialogLayout) -> {
                JFXButton saveButton = new JFXButton("保存");
                saveButton.getStyleClass().add("dialog-accept");
                saveButton.setOnAction(e1->jfxAlert1.close());
                return Arrays.asList(saveButton);
            });
            jfxAlert.show();
        });
    }
}
